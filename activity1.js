db.users.insertMany([
	{
		"firstName" : "Diane",
		"lastName" : "Murphy",
		"email" : "dmurphy@mail.com",
		"isAdmin" : false, 
		"isActive" : true
	},
	{
		"firstName" : "Mary",
		"lastName" : "Patterson",
		"email" : "mpatterson@mail.com",
		"isAdmin" : false, 
		"isActive" : true
	},
	{
		"firstName" : "Jeff",
		"lastName" : "Firrelli",
		"email" : "jfirrelli@mail.com",
		"isAdmin" : false, 
		"isActive" : true
	},
	{
		"firstName" : "Gerard",
		"lastName" : "Bondur",
		"email" : "gbondur@mail.com",
		"isAdmin" : false, 
		"isActive" : true
	},
	{
		"firstName" : "Pamela",
		"lastName" : "Castillo",
		"email" : "pcastillo@mail.com",
		"isAdmin" : true, 
		"isActive" : false
	},
	{
		"firstName" : "George",
		"lastName" : "Vanauf",
		"email" : "gvanauf@mail.com",
		"isAdmin" : true, 
		"isActive" : true
	},


])


db.courses.insertMany([

	{
		"name" : "Professional Development", 
		"price" : 10000
	},
	{
		"name" : "Business Processing", 
		"price" : 13000
	},

])


db.users.find({"isAdmin" : false})


db.courses.updateOne(
    {"name" : "Professional Development"},
    {
            $set : {
                "enrollees" : [
                    {
                        "userId" : ObjectId("620cccaff7ac8eb584cf45bd")
                    },
                    {
                        "userId" : ObjectId("620cccaff7ac8eb584cf45be")
                    },
                    
                ]
            }
    }
)

db.courses.updateOne(
    {"name" : "Business Processing"},
    {
            $set : {
                "enrollees" : [
                    {
                        "userId" : ObjectId("620cccaff7ac8eb584cf45bf")
                    },
                    {
                        "userId" : ObjectId("620cccaff7ac8eb584cf45c0")
                    },
                    
                ]
            }
    }
)
